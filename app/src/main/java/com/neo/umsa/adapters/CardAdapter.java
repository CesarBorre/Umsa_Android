package com.neo.umsa.adapters;

import com.neo.umsa.model.Certificados;
import com.neo.umsa.model.DataWS;
import com.neo.umsa.model.Titulos_WS_Data;
import com.neo.umsa.subfragments.FrenteFragment;
import com.neo.umsa.subfragments.HistorialFragment;
import com.neo.umsa.subfragments.ReversoFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class CardAdapter extends FragmentPagerAdapter{
	DataWS dataWs;
	Titulos_WS_Data titulos;
	Certificados certificados;
	String tabs [] = {"FRENTE", "REVERSO", "HISTORIAL ACADÉMICO"};

	public CardAdapter(FragmentManager fm, DataWS dataWs, Titulos_WS_Data titulos, Certificados certificados) {
		super(fm);
		this.dataWs = dataWs;
		this.certificados = certificados;
		this.titulos = titulos;
	}

	@Override
	public Fragment getItem(int position) {
		Fragment fragment = null;
		switch (position) {
		case 0:
			Bundle bundle = new Bundle();
			bundle.putSerializable("dataWs", dataWs);
			bundle.putSerializable("titulos", titulos);
			fragment = new FrenteFragment();
			fragment.setArguments(bundle);
			break;

		case 1:
			fragment = new ReversoFragment();
			break;
			
		case 2:
			fragment = new HistorialFragment();
			Bundle args = new Bundle();
			args.putSerializable("dataWs", dataWs);
			args.putSerializable("titulos", titulos);
			args.putParcelable("certificados", certificados);
			fragment.setArguments(args);
			break;
		}
		return fragment;
	}
	
	@Override
	public CharSequence getPageTitle(int position) {
		return tabs[position];
	}

	@Override
	public int getCount() {
		return 3;
	}

}
