package com.neo.umsa;

import com.neo.umsa.adapters.CardAdapter;
import com.neo.umsa.model.Certificados;
import com.neo.umsa.model.DataWS;
import com.neo.umsa.model.Titulos_WS_Data;
import com.neo.umsa.tabs.SlidingTabLayout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class FragmentActivity extends AppCompatActivity{
	DataWS dataWs;
	Titulos_WS_Data titulos;
	Certificados certificados;
	
	private ViewPager pager;
    private SlidingTabLayout mTabs;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fragment);
		Intent intent = getIntent();
		dataWs = (DataWS)intent.getSerializableExtra("dataWs");
		Log.d(null, "Nombre "+dataWs.getNombre());
		titulos = (Titulos_WS_Data)intent.getSerializableExtra("titulos");
		certificados = intent.getParcelableExtra("certificados");
		slidingBar_Pager();
	}
	
	private void slidingBar_Pager() {
        pager = (ViewPager) findViewById(R.id.pagerID);
        pager.setAdapter(new CardAdapter(getSupportFragmentManager(), dataWs, titulos, certificados));
        mTabs = (SlidingTabLayout) findViewById(R.id.tabs);
        mTabs.setDistributeEvenly(true);
        mTabs.setViewPager(pager);
    }

}
