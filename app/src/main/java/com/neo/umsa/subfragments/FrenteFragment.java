package com.neo.umsa.subfragments;

import com.neo.umsa.R;
import com.neo.umsa.model.DataWS;
import com.neo.umsa.model.Titulos_WS_Data;
import com.neo.umsa.utils.ImageUtil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class FrenteFragment extends Fragment {
	TextView nombreUni, ci, nombre, carrera;
	ImageView escudo, foto;
	DataWS data;
	Titulos_WS_Data titulos;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		data = (DataWS) getArguments().getSerializable("dataWs");
		titulos = (Titulos_WS_Data) getArguments().getSerializable("titulos");
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.frente_fragment, container, false);
		nombreUni = (TextView) v.findViewById(R.id.nombreUniID);
		escudo = (ImageView) v.findViewById(R.id.escudo);
		escudo.setAlpha(30);
		setFont(nombreUni);
		ci = (TextView) v.findViewById(R.id.ciID);
		ci.setText("C.I. " + data.getCi());
		nombre = (TextView) v.findViewById(R.id.nombreFrenteID);
		nombre.setText(data.getNombre());
		carrera = (TextView) v.findViewById(R.id.carreraID);
		carrera.setText(data.getCarrera());
		foto = (ImageView) v.findViewById(R.id.fotoID);
		ImageUtil.setImg(titulos.getFotoOriginal(), foto);
		return v;
	}

	private void setFont(TextView t) {
		Typeface type = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/tabarra.otf");
		t.setTypeface(type);
	}
}
