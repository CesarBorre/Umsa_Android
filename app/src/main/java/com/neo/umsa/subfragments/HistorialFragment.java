package com.neo.umsa.subfragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;

import com.neo.umsa.R;
import com.neo.umsa.model.Certificados;
import com.neo.umsa.model.DataWS;
import com.neo.umsa.model.Titulos_WS_Data;
import com.neo.umsa.utils.DateUtil;
import com.neo.umsa.utils.ImageUtil;

import java.util.Date;

public class HistorialFragment extends Fragment {
    Titulos_WS_Data titulos;
    Certificados certificados;
    DataWS data;
    TextView noCuenta, nombre, anioIngreso, plantel, carrera, planEstudios, obligatorios, optativos, credTotales, aprobadas, noAprobadas, totalAsignaturas, promedio;
    ImageView foto;
    HorizontalScrollView table, table2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        data = (DataWS) getArguments().getSerializable("dataWs");
        titulos = (Titulos_WS_Data) getArguments().getSerializable("titulos");
        certificados = getArguments().getParcelable("certificados");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.historial_fragment, container, false);
        noCuenta = (TextView) v.findViewById(R.id.numeroCtaID);
        noCuenta.setText(data.getCi());
        nombre = (TextView) v.findViewById(R.id.nombreHIstorialID);
        nombre.setText(data.getNombre());
        plantel = (TextView) v.findViewById(R.id.plantelID);
        plantel.setText(certificados.getPlantel());
        carrera = (TextView) v.findViewById(R.id.carreraHistorialID);
        carrera.setText(String.valueOf(certificados.getIntPlanEstudios()));
        planEstudios = (TextView) v.findViewById(R.id.planEstudiosID);
        planEstudios.setText(certificados.getCarrera());
        obligatorios = (TextView) v.findViewById(R.id.obligatortiosID);
        obligatorios.setText(String.valueOf(certificados.getIntCreditosObligatorios()));
        optativos = (TextView) v.findViewById(R.id.optativosID);
        optativos.setText(String.valueOf(certificados.getIntCreditosOptativos()));
        credTotales = (TextView) v.findViewById(R.id.credTotalesID);
        credTotales.setText(String.valueOf(certificados.getIntCreditosTotales()));
        aprobadas = (TextView) v.findViewById(R.id.aprovadasID);
        aprobadas.setText(String.valueOf(certificados.getIntAsignaturasAprobadas()));
        noAprobadas = (TextView) v.findViewById(R.id.noAprobadasID);
        noAprobadas.setText(String.valueOf(certificados.getIntAsignaturasNoAprobadas()));
        totalAsignaturas = (TextView) v.findViewById(R.id.asgnaturasTotalesID);
        totalAsignaturas.setText(String.valueOf(certificados.getIntAsignaturasTotal()));
        promedio = (TextView) v.findViewById(R.id.promedioID);
        promedio.setText(String.valueOf(certificados.getdPromedio()));
        anioIngreso = (TextView) v.findViewById(R.id.anioIngresoID);
        anioIngreso.setText(DateUtil.dateToString(new Date(certificados.getFechaIngreso()), "dd-MM-yyyy"));
        foto = (ImageView) v.findViewById(R.id.fotoHistorialID);
        ImageUtil.setImg(titulos.getFotoOriginal(), foto);
        table = (HorizontalScrollView) v.findViewById(R.id.table1);
        table2 = (HorizontalScrollView) v.findViewById(R.id.table2);
        if (data.getCi().equals("304167751")) {
            table.setVisibility(View.VISIBLE);
            table2.setVisibility(View.GONE);
        } else {
            table.setVisibility(View.GONE);
            table2.setVisibility(View.VISIBLE);
        }
        return v;
    }

}
