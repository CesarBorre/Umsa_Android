package com.neo.umsa.model;

import android.os.Parcel;
import android.os.Parcelable;

public class DataChip implements Parcelable{
	
	private String carrera;
	private String nombre;
	private String folio;
	private String ci;
	
	public DataChip (
			String carrera,
			String nombre,
			String folio, 
			String ci){
		this.carrera = carrera;
		this.nombre = nombre;
		this.folio = folio;
		this.ci = ci;		
	}	
	
	protected DataChip(Parcel in) {
		carrera = in.readString();
		nombre = in.readString();
		folio = in.readString();
		ci = in.readString();
	}
	
	public static final Creator<DataChip> CREATOR = new Creator<DataChip>() {
        @Override
        public DataChip createFromParcel(Parcel in) {
            return new DataChip(in);
        }

        @Override
        public DataChip[] newArray(int size) {
            return new DataChip[size];
        }
    };
	
	public String getCarrera() {
		return carrera;
	}
	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getCi() {
		return ci;
	}
	public void setCi(String ci) {
		this.ci = ci;
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(carrera);
		dest.writeString(nombre);
		dest.writeString(folio);
		dest.writeString(ci);		
	}
}
