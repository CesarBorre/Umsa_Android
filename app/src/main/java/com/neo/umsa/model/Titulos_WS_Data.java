package com.neo.umsa.model;

import java.io.Serializable;

/**
 * Created by LeviAcosta on 28/06/2016.
 */

public class Titulos_WS_Data implements Serializable {
	private int numIdentificacion;
	private String nombre;
	private String apePaterno;
	private String apeMaterno;
	private String titulo;
	private String carrera;
	private String nivel;
	private int numFOlioDocumento;
	private String nacionalidad;
	private String genero;
	private long fechaNac;
	private long fechaExpedicion;
	private String nombreUniversidad;
	private int claveUNiversidad;
	private byte[] fotoOriginal;
	private byte[] fotoMiniatura;
	private byte[] firmaDigital;

	public Titulos_WS_Data(int numIdentificacion, String nombre,
			String apePaterno, String apeMaterno, String titulo,
			String carrera, String nivel, int numFOlioDocumento,
			String nacionalidad, String genero, long fechaNac,
			long fechaExpedicion, String nombreUniversidad,
			int claveUNiversidad, byte[] fotoOriginal, byte[] fotoMiniatura,
			byte[] firmaDigital) {
		this.numIdentificacion = numIdentificacion;
		this.nombre = nombre;
		this.apePaterno = apePaterno;
		this.apeMaterno = apeMaterno;
		this.titulo = titulo;
		this.carrera = carrera;
		this.nivel = nivel;
		this.numFOlioDocumento = numFOlioDocumento;
		this.nacionalidad = nacionalidad;
		this.genero = genero;
		this.fechaNac = fechaNac;
		this.fechaExpedicion = fechaExpedicion;
		this.nombreUniversidad = nombreUniversidad;
		this.claveUNiversidad = claveUNiversidad;
		this.fotoOriginal = fotoOriginal;
		this.fotoMiniatura = fotoMiniatura;
		this.firmaDigital = firmaDigital;
	}

	public Titulos_WS_Data(byte[] fotoOriginal) {
		this.fotoOriginal = fotoOriginal;
	}

	public int getNumIdentificacion() {
		return numIdentificacion;
	}

	public void setNumIdentificacion(int numIdentificacion) {
		this.numIdentificacion = numIdentificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApePaterno() {
		return apePaterno;
	}

	public void setApePaterno(String apePaterno) {
		this.apePaterno = apePaterno;
	}

	public String getApeMaterno() {
		return apeMaterno;
	}

	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = apeMaterno;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getCarrera() {
		return carrera;
	}

	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public int getNumFOlioDocumento() {
		return numFOlioDocumento;
	}

	public void setNumFOlioDocumento(int numFOlioDocumento) {
		this.numFOlioDocumento = numFOlioDocumento;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public long getFechaNac() {
		return fechaNac;
	}

	public void setFechaNac(long fechaNac) {
		this.fechaNac = fechaNac;
	}

	public long getFechaExpedicion() {
		return fechaExpedicion;
	}

	public void setFechaExpedicion(long fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}

	public String getNombreUniversidad() {
		return nombreUniversidad;
	}

	public void setNombreUniversidad(String nombreUniversidad) {
		this.nombreUniversidad = nombreUniversidad;
	}

	public int getClaveUNiversidad() {
		return claveUNiversidad;
	}

	public void setClaveUNiversidad(int claveUNiversidad) {
		this.claveUNiversidad = claveUNiversidad;
	}

	public byte[] getFotoOriginal() {
		return fotoOriginal;
	}

	public void setFotoOriginal(byte[] fotoOriginal) {
		this.fotoOriginal = fotoOriginal;
	}

	public byte[] getFotoMiniatura() {
		return fotoMiniatura;
	}

	public void setFotoMiniatura(byte[] fotoMiniatura) {
		this.fotoMiniatura = fotoMiniatura;
	}

	public byte[] getFirmaDigital() {
		return firmaDigital;
	}

	public void setFirmaDigital(byte[] firmaDigital) {
		this.firmaDigital = firmaDigital;
	}
}
