package com.neo.umsa.model;

import java.io.Serializable;

public class DataWS implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -318336291333729307L;
	private String carrera;
	private String nombre;
	private String folio;
	private String ci;
	private int id;
	
	public DataWS (String carrera, String nombre, String folio, String ci, int id) {
		this.carrera = carrera;
		this.nombre = nombre;
		this.folio =  folio;
		this.ci = ci;
		this.id = id;
	}
	
	public String getCarrera() {
		return carrera;
	}
	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getCi() {
		return ci;
	}
	public void setCi(String ci) {
		this.ci = ci;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
