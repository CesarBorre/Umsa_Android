package com.neo.umsa.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Certificados implements Parcelable {

    public static final Creator<Certificados> CREATOR = new Creator<Certificados>() {
        @Override
        public Certificados createFromParcel(Parcel in) {
            return new Certificados(in);
        }

        @Override
        public Certificados[] newArray(int size) {
            return new Certificados[size];
        }
    };
    private int intNumeroIdentificacionCert;
    private int intPlanEstudios;
    private int intCreditosObligatorios;
    private int intCreditosOptativos;
    private int intCreditosTotales;
    private int intAsignaturasAprobadas;
    private int intAsignaturasNoAprobadas;
    private int intAsignaturasTotal;
    private double dPromedio;
    private String carrera;
    private String plantel;
    private long fechaIngreso;

    public Certificados(int intNumeroIdentificacionCert,
                        int intPlanEstudios,
                        int intCreditosObligatorios,
                        int intCreditosOptativos,
                        int intCreditosTotales,
                        int intAsignaturasAprobadas,
                        int intAsignaturasNoAprobadas,
                        int intAsignaturasTotal,
                        double dPromedio,
                        String carrera,
                        String plantel,
                        long fechaIngreso) {
        this.intNumeroIdentificacionCert = intNumeroIdentificacionCert;
        this.intPlanEstudios = intPlanEstudios;
        this.intCreditosObligatorios = intCreditosObligatorios;
        this.intCreditosOptativos = intCreditosOptativos;
        this.intCreditosTotales = intCreditosTotales;
        this.intAsignaturasAprobadas = intAsignaturasAprobadas;
        this.intAsignaturasNoAprobadas = intAsignaturasNoAprobadas;
        this.intAsignaturasTotal = intAsignaturasTotal;
        this.dPromedio = dPromedio;
        this.carrera = carrera;
        this.plantel = plantel;
        this.fechaIngreso = fechaIngreso;
    }

    protected Certificados(Parcel in) {
        intNumeroIdentificacionCert = in.readInt();
        intPlanEstudios = in.readInt();
        intCreditosObligatorios = in.readInt();
        intCreditosOptativos = in.readInt();
        intCreditosTotales = in.readInt();
        intAsignaturasAprobadas = in.readInt();
        intAsignaturasNoAprobadas = in.readInt();
        intAsignaturasTotal = in.readInt();
        dPromedio = in.readDouble();
        carrera = in.readString();
        plantel = in.readString();
        fechaIngreso = in.readLong();
    }

    public int getIntNumeroIdentificacionCert() {
        return intNumeroIdentificacionCert;
    }

    public void setIntNumeroIdentificacionCert(int intNumeroIdentificacionCert) {
        this.intNumeroIdentificacionCert = intNumeroIdentificacionCert;
    }

    public int getIntPlanEstudios() {
        return intPlanEstudios;
    }

    public void setIntPlanEstudios(int intPlanEstudios) {
        this.intPlanEstudios = intPlanEstudios;
    }

    public int getIntCreditosObligatorios() {
        return intCreditosObligatorios;
    }

    public void setIntCreditosObligatorios(int intCreditosObligatorios) {
        this.intCreditosObligatorios = intCreditosObligatorios;
    }

    public int getIntCreditosOptativos() {
        return intCreditosOptativos;
    }

    public void setIntCreditosOptativos(int intCreditosOptativos) {
        this.intCreditosOptativos = intCreditosOptativos;
    }

    public int getIntCreditosTotales() {
        return intCreditosTotales;
    }

    public void setIntCreditosTotales(int intCreditosTotales) {
        this.intCreditosTotales = intCreditosTotales;
    }

    public int getIntAsignaturasAprobadas() {
        return intAsignaturasAprobadas;
    }

    public void setIntAsignaturasAprobadas(int intAsignaturasAprobadas) {
        this.intAsignaturasAprobadas = intAsignaturasAprobadas;
    }

    public int getIntAsignaturasNoAprobadas() {
        return intAsignaturasNoAprobadas;
    }

    public void setIntAsignaturasNoAprobadas(int intAsignaturasNoAprobadas) {
        this.intAsignaturasNoAprobadas = intAsignaturasNoAprobadas;
    }

    public int getIntAsignaturasTotal() {
        return intAsignaturasTotal;
    }

    public void setIntAsignaturasTotal(int intAsignaturasTotal) {
        this.intAsignaturasTotal = intAsignaturasTotal;
    }

    public double getdPromedio() {
        return dPromedio;
    }

    public void setdPromedio(double dPromedio) {
        this.dPromedio = dPromedio;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(intNumeroIdentificacionCert);
        parcel.writeInt(intPlanEstudios);
        parcel.writeInt(intCreditosObligatorios);
        parcel.writeInt(intCreditosOptativos);
        parcel.writeInt(intCreditosTotales);
        parcel.writeInt(intAsignaturasAprobadas);
        parcel.writeInt(intAsignaturasNoAprobadas);
        parcel.writeInt(intAsignaturasTotal);
        parcel.writeDouble(dPromedio);
        parcel.writeString(carrera);
        parcel.writeString(plantel);
        parcel.writeLong(fechaIngreso);
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getPlantel() {
        return plantel;
    }

    public void setPlantel(String plantel) {
        this.plantel = plantel;
    }

    public long getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(long fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }
}