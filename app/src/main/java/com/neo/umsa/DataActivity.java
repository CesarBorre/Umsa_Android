package com.neo.umsa;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Credentials;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neo.umsa.model.Certificados;
import com.neo.umsa.model.DataChip;
import com.neo.umsa.model.DataWS;
import com.neo.umsa.model.Titulos_WS_Data;

public class DataActivity extends AppCompatActivity {
	public static final String TAG = DataActivity.class.getSimpleName();

	DataChip dataChip;

	ProgressDialog progressDialog;
	Button buscarBtn;

	TextView nombre, carrera, folio, ci;

	ImageView fotoChip;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_data);
		Intent intent = getIntent();
		dataChip = intent.getParcelableExtra("data");
//		init();
	}

	private void init() {
//		progressDialog = new ProgressDialog(DataActivity.this);
//		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//		progressDialog.setCanceledOnTouchOutside(false);
//		progressDialog.setTitle(getResources().getString(
//				R.string.title_progress_consulta));
//		progressDialog.setMessage(getResources().getString(
//				R.string.msg_progress));
//		buscarBtn = (Button) findViewById(R.id.buscarID);
//		nombre = (TextView) findViewById(R.id.nombreId);
//		nombre.setText(dataChip.getNombre());
//		carrera = (TextView) findViewById(R.id.carreraID);
//		carrera.setText(dataChip.getCarrera());
//		folio = (TextView) findViewById(R.id.folioID);
//		folio.setText(dataChip.getFolio());
//		fotoChip = (ImageView) findViewById(R.id.fotoChipID);
//		if (dataChip.getFolio().equals("1713896")) {
//			fotoChip.setImageDrawable(getResources().getDrawable(
//					R.drawable.titulo2_bn));
//		}
//		ci = (TextView) findViewById(R.id.ciID);
//		ci.setText(dataChip.getCi());
//		actions();
	}

	private void actions() {
		buscarBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				searchInfoWS();
			}
		});
	}

	private void searchInfoWS() {
		progressDialog.show();
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(
				Request.Method.GET,
				"http://mobile.neology-demos.com:8080/api/identificacion?folioIdentificacion="
						+ dataChip.getFolio(), null,
				new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						Log.d(TAG, response.toString());
						new WS().execute(response.toString());
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						progressDialog.dismiss();
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						Toast.makeText(getApplicationContext(),
								"Error al conectar con servidor",
								Toast.LENGTH_SHORT).show();
					}
				});

		// Adding request to request queue
		VolleyApp.getmInstance().addToRequestQueue(jsonObjReq);
	}

	class WS extends AsyncTask<String, Void, Boolean> {

		DataWS dataWs;

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				JSONObject object = new JSONObject(params[0]);
				JSONObject jsonObject = object
						.getJSONObject("identificaciones");
				dataWs = new DataWS(
						jsonObject.getString("carrera"),
						jsonObject.getString("apePaterno") + " "
								+ jsonObject.getString("apeMaterno") + " "
								+ jsonObject.getString("nombre"),
						String.valueOf(jsonObject.getInt("folioIdentificacion")),
						String.valueOf(jsonObject.getInt("ci")), jsonObject
								.getInt("id"));

			} catch (JSONException e) {
				e.printStackTrace();
				return false;
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			if (result) {
				searchHistorial(dataWs);
			}

		}

	}

	private void searchHistorial(final DataWS data) {
		progressDialog.show();
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(
				Request.Method.GET,
				"http://mobile.neology-demos.com:8080/api/titulo?intNumeroIdentificacion="
						+ data.getId(), null,
				new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						Log.d(TAG, response.toString());
						new WS_HIstorial(data).execute(response.toString());
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						progressDialog.dismiss();
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						Toast.makeText(getApplicationContext(),
								"Error al conectar con servidor",
								Toast.LENGTH_SHORT).show();
					}
				});

		// Adding request to request queue
		VolleyApp.getmInstance().addToRequestQueue(jsonObjReq);
	}

	class WS_HIstorial extends AsyncTask<String, Void, Boolean> {

		Certificados certificados;
		Titulos_WS_Data titulos;
		DataWS data;

		public WS_HIstorial(DataWS data) {
			this.data = data;
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				JSONObject object = new JSONObject(params[0]);
				JSONObject jsonObject = object.getJSONObject("titulos");
				titulos = new Titulos_WS_Data(jsonObject.getString(
						"bFotoOriginal").getBytes());
				JSONObject jsonCert = jsonObject.getJSONObject("certificados");
				certificados = new Certificados(
						jsonCert.getInt("intNumeroIdentificacionCert"),
						jsonCert.getInt("intPlanEstudios"),
						jsonCert.getInt("intCreditosObligatorios"),
						jsonCert.getInt("intCreditosOptativos"),
						jsonCert.getInt("intCreditosTotales"),
						jsonCert.getInt("intAsignaturasAprobadas"),
						jsonCert.getInt("intAsignaturasNoAprobadas"),
						jsonCert.getInt("intAsignaturasTotal"),
						jsonCert.getDouble("dPromedio"),
						jsonCert.getString("carrera"),
						jsonCert.getString("plantel"),
						jsonCert.getLong("fechaIngreso"));
			} catch (JSONException e) {
				e.printStackTrace();
				return false;
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			if (result) {
				Intent intent = new Intent(getApplicationContext(),
						FragmentActivity.class);
				intent.putExtra("dataWs", data);
				intent.putExtra("certificados", certificados);
				intent.putExtra("titulos", titulos);
				startActivity(intent);
			}

		}

	}

}
