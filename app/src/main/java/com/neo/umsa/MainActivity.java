package com.neo.umsa;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.nfc.NfcAdapter;
import android.nfc.tech.NfcF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neo.umsa.lecturanfc.LecturaTag;
import com.neo.umsa.model.Certificados;
import com.neo.umsa.model.DataChip;
import com.neo.umsa.model.DataWS;
import com.neo.umsa.model.Titulos_WS_Data;
import com.neo.umsa.utils.SnackBar;

public class MainActivity extends Activity {
	public static final String TAG = MainActivity.class.getSimpleName();

	MainActivity mActivity;
	NfcAdapter nfcAdapter = null;
	PendingIntent pendingIntent = null;
	IntentFilter[] filters = null;
	String[][] techList = null;
	protected String[] datosTag;
	Intent lecturaIntent;

	DataChip data;

	LinearLayout present, tarjeta;
	TextView nombre, carrera, folio, ci;
	Button consulta;
	ImageView foto;
	ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_read);
		present = (LinearLayout) findViewById(R.id.presentancionID);
		tarjeta = (LinearLayout) findViewById(R.id.tarjetaID);
		nombre = (TextView) findViewById(R.id.nombreID);
		carrera = (TextView) findViewById(R.id.carreraID);
		folio = (TextView) findViewById(R.id.folioID);
		ci = (TextView) findViewById(R.id.ciID);
		foto = (ImageView) findViewById(R.id.fotoChipID);
		consulta = (Button)findViewById(R.id.buscarID);
		
		
		progressDialog = new ProgressDialog(MainActivity.this);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.setTitle(getResources().getString(
				R.string.title_progress_consulta));
		progressDialog.setMessage(getResources().getString(
				R.string.msg_progress));
		
		
		// Obtenemos el control sobre el lector de NFC
		nfcAdapter = NfcAdapter.getDefaultAdapter(this);
		// Si no se encuentra el lector de NFC se cierra aplicacion
		if (nfcAdapter == null) {
			Toast.makeText(this, getString(R.string.nfc_warning),
					Toast.LENGTH_LONG).show();
			finish();
			return;

		} else if(!nfcAdapter.isEnabled()){
			SnackBar.showSnackBar(getResources().getString(R.string.no_nfc), this, 0, R.id.presentancionID);
		} else {
			// Creamos un Intent para manejar los datos leidos
			pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
					getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

			// Creamos un filtro de Intent relacionado con descubrir un mensaje NDEF
			IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
			IntentFilter discovery = new IntentFilter(
					NfcAdapter.ACTION_TAG_DISCOVERED);

			// Configuramos el filtro para que acepte de cualquier tipo de NDEF
			try {
				ndef.addDataType("*/*");
			} catch (MalformedMimeTypeException e) {
				throw new RuntimeException("fail", e);
			}
			filters = new IntentFilter[] { ndef, discovery };

			// Configuramos para que lea de cualquier clase de tag NFC
			techList = new String[][] { new String[] { NfcF.class.getName() } };

			Intent lecturaIntent = getIntent();
			Log.d(TAG, "INTENT " + lecturaIntent.toString());
			Log.d(TAG, "PENDING INTENT " + pendingIntent.toString());
			Parcelable[] rawMsgs = lecturaIntent
					.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
			Log.d(TAG, "RAW " + rawMsgs);

			if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(lecturaIntent.getAction())) {
				lecturaNFC(lecturaIntent);
			}
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.d(TAG, "onStart()");
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.d(TAG, "onRestart()");
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.d(TAG, "onResume()");
		// nfcAdapter.enableForegroundDispatch(this, pendingIntent, filters,
		// techList);
		// lecturaNFC(lecturaIntent);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Log.d(TAG, "onPause()");
		nfcAdapter.disableForegroundDispatch(this);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		Log.d(TAG, "onStop()");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy()");
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		Log.d(TAG, "onNewIntent()");
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		lecturaNFC(intent);
	}

	public void lecturaNFC(Intent intent) {

		present.setVisibility(View.GONE);
		try {

			LecturaTag lecturaObj = new LecturaTag(intent);
			datosTag = lecturaObj.lectura();

			if (datosTag != null) {

				Log.d(TAG, "# Datos leidos: " + datosTag.length);
				int i = 0;
				while (i < datosTag.length) {
					Log.d(TAG, "Dato[" + i + "]: " + datosTag[i]);
					i++;
				}
				present.setVisibility(View.GONE);
				tarjeta.setVisibility(View.VISIBLE);
				nombre.setText(datosTag[1]);
				carrera.setText(datosTag[0]);
				folio.setText(datosTag[2]);
				ci.setText(datosTag[3]);
				if (datosTag[2].equals("1713896")) {
					foto.setImageDrawable(getResources().getDrawable(
							R.drawable.titulo2_bn));
				} else if (datosTag[2].equals("1713895")) {
					foto.setImageDrawable(getResources().getDrawable(
							R.drawable.mujer_bn));
				}
				actions();
			} else {

				Log.i(TAG, "Lectura de datos no validos");
				// linearLayout.setVisibility(View.GONE);

			}
		} catch (Exception e) {
			Log.e(TAG, "Excepion::lecturaNFC->" + e.getMessage());
			e.printStackTrace();
		}

	}
	
	private void actions() {
		consulta.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				searchInfoWS();
			}
		});
	}
	
	private void searchInfoWS() {
		progressDialog.show();
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(
				Request.Method.GET,
				"http://mobile.neology-demos.com:8080/api/identificacion?folioIdentificacion="
						+ datosTag[2], null,
				new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						Log.d(TAG, response.toString());
						new WS().execute(response.toString());
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						progressDialog.dismiss();
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						Toast.makeText(getApplicationContext(),
								"Error al conectar con servidor",
								Toast.LENGTH_SHORT).show();
					}
				});

		// Adding request to request queue
		VolleyApp.getmInstance().addToRequestQueue(jsonObjReq);
	}

	class WS extends AsyncTask<String, Void, Boolean> {

		DataWS dataWs;

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				JSONObject object = new JSONObject(params[0]);
				JSONObject jsonObject = object
						.getJSONObject("identificaciones");
				dataWs = new DataWS(
						jsonObject.getString("carrera"),
						jsonObject.getString("apePaterno") + " "
								+ jsonObject.getString("apeMaterno") + " "
								+ jsonObject.getString("nombre"),
						String.valueOf(jsonObject.getInt("folioIdentificacion")),
						String.valueOf(jsonObject.getInt("ci")), jsonObject
								.getInt("id"));

			} catch (JSONException e) {
				e.printStackTrace();
				return false;
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				searchHistorial(dataWs);
			}

		}

	}

	private void searchHistorial(final DataWS data) {
		progressDialog.show();
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(
				Request.Method.GET,
				"http://mobile.neology-demos.com:8080/api/titulo?intNumeroIdentificacion="
						+ data.getId(), null,
				new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						Log.d(TAG, response.toString());
						new WS_HIstorial(data).execute(response.toString());
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						progressDialog.dismiss();
						VolleyLog.e(TAG, "Error: " + error.getMessage());
						Toast.makeText(getApplicationContext(),
								"Error al conectar con servidor",
								Toast.LENGTH_SHORT).show();
					}
				});

		// Adding request to request queue
		VolleyApp.getmInstance().addToRequestQueue(jsonObjReq);
	}

	class WS_HIstorial extends AsyncTask<String, Void, Boolean> {

		Certificados certificados;
		Titulos_WS_Data titulos;
		DataWS data;

		public WS_HIstorial(DataWS data) {
			this.data = data;
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				JSONObject object = new JSONObject(params[0]);
				JSONObject jsonObject = object.getJSONObject("titulos");
				titulos = new Titulos_WS_Data(jsonObject.getString(
						"bFotoOriginal").getBytes());
				JSONObject jsonCert = jsonObject.getJSONObject("certificados");
				certificados = new Certificados(
						jsonCert.getInt("intNumeroIdentificacionCert"),
						jsonCert.getInt("intPlanEstudios"),
						jsonCert.getInt("intCreditosObligatorios"),
						jsonCert.getInt("intCreditosOptativos"),
						jsonCert.getInt("intCreditosTotales"),
						jsonCert.getInt("intAsignaturasAprobadas"),
						jsonCert.getInt("intAsignaturasNoAprobadas"),
						jsonCert.getInt("intAsignaturasTotal"),
						jsonCert.getDouble("dPromedio"),
						jsonCert.getString("carrera"),
						jsonCert.getString("plantel"),
						jsonCert.getLong("fechaIngreso"));
			} catch (JSONException e) {
				e.printStackTrace();
				return false;
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			if (result) {
				Intent intent = new Intent(getApplicationContext(),
						FragmentActivity.class);
				intent.putExtra("dataWs", data);
				intent.putExtra("certificados", certificados);
				intent.putExtra("titulos", titulos);
				startActivity(intent);
			}

		}

	}
}
